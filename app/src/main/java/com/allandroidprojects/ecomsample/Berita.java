package com.allandroidprojects.ecomsample;

import java.util.ArrayList;
import java.util.List;


public class Berita {
    private int resId;
    private String breed;
    private String description;

    List<Berita> Berita = new ArrayList<>();

    public Berita() {

    }

    public Berita(int resId, String breed, String description) {
        this.resId = resId;
        this.breed = breed;
        this.description = description;
    }
    Berita(int resId, String breed) {
        this.resId = resId;
        this.breed = breed;

    }


    public int getResId() {
        return resId;
    }

    public void setResId(int resId) {
        this.resId = resId;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Berita> getBeritas() {
        return Berita;
    }

    public void setBeritas(List<Berita> Beritas) {
        this.Berita = Beritas;
    }
}

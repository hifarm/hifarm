package com.allandroidprojects.ecomsample;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by ayu a kartika on 1/8/2018.
 */

public class ListAdapter extends BaseAdapter
{
    Context context;
    //Berita Beritas = new Berita();
    Berita Beritas;
    private LayoutInflater inflater;
    public ListAdapter(Context context, Berita Beritas) {
        this.context = context;
        this.Beritas = Beritas;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        if (Beritas == null)
            return 0;
        if (Beritas.getBeritas() == null)
            return 0;

        return Beritas.getBeritas().size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;

//        LayoutInflater mInflater =
//                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.list_itemberita, parent, false);
//        if(convertView != null){
//            view = convertView;
//        }else {
//            view = mInflater.inflate(R.layout.list_item, parent, false);
//        }

        TextView textView = (TextView) view.findViewById(R.id.title);
        ImageView imageView = (ImageView) view.findViewById(R.id.image);
        TextView textDesc = (TextView) view.findViewById(R.id.desc);


        if (Beritas != null && Beritas.getBeritas() != null){
            textView.setText(Beritas.getBeritas().get(position).getBreed());
            imageView.setImageResource(Beritas.getBeritas().get(position).getResId());
            textDesc.setText(Beritas.getBeritas().get(position).getDescription());
        }


//        if (convertView != null)
//            view = convertView;
        return view;


    }
}

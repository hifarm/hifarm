package com.allandroidprojects.ecomsample.login;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.allandroidprojects.ecomsample.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.blank_example);

        String userFromIntent = getIntent().getStringExtra("USERNAME");
        TextView textViewUsername = (TextView) findViewById(R.id.textUsername);
        textViewUsername.setText(userFromIntent);
    }
}

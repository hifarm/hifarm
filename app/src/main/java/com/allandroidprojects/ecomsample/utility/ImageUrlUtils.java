package com.allandroidprojects.ecomsample.utility;

import java.util.ArrayList;

public class ImageUrlUtils {
    static ArrayList<String> wishlistImageUri = new ArrayList<>();
    static ArrayList<String> wishlistNameUri = new ArrayList<>();
    static ArrayList<String> wishlistLokasiUri = new ArrayList<>();
    static ArrayList<String> wishlistLuasUri = new ArrayList<>();

    static ArrayList<String> cartListImageUri = new ArrayList<>();
    static ArrayList<String> cartlistNameUri = new ArrayList<>();
    static ArrayList<String> cartlistLokasiUri = new ArrayList<>();
    static ArrayList<String> cartlistLuasUri = new ArrayList<>();

    public static String[] getImageUrls() {
        String[] urls = new String[] {
                "http://images1.rri.co.id/thumbs/berita_336524_800x600_buah_naga.jpg",
                "http://www.sumut24.co/wp-content/uploads/2016/02/petani-buah.jpg",
                "http://www.mongabay.co.id/wp-content/uploads/2016/02/kakao3-Petani-di-Kecamatan-Wotu-memilih-buah-kakao-yang-sehat-dari-hasil-binaan-PT-Mars-FOTO-OLEH-IQBAL-LUBIS.jpg",
                "http://ocmcdn.oomph.co.id/uploads/shareit_contents/2017/11/03/28681/8_20171103022643_petanistrawberi.jpg",
                "http://st294294.sitekno.com/images/art_43645.jpg",
                "http://v-images2.antarafoto.com/petani-blewah-m6tu97-prv.jpg",
                "https://static.simomot.com/wp-content/uploads/2015/01/antarafoto-panen-labu-220115-da.jpg",
                "https://bulelengkab.go.id/assets/images/berita/Gerokgak-Pusat-Budi-Daya-Pisang-Ijo_092326.jpg",
                "http://hariansib.co/photo/dir022016/hariansib_Hama-Lalat-Buah-Mengganas--Petani-Jeruk-di-Dairi-Terancam-Gagal-Panen.jpg",
                "https://tabloidsergap.files.wordpress.com/2013/05/apel-kota-batu.jpg",
                "http://beritadaerah.co.id/wp-content/uploads/2014/10/Panen-Pepaya-221014-RM-3.jpg",
                "https://khazanahleuser.files.wordpress.com/2013/03/sam_02291.jpg?w=500",

        };
        return urls;
    }

    public static String[] getSayurDana() {
        String[] urls = new String[] {
                "Rp. 60.500.000",
                "Rp. 70.700.000",
                "Rp. 80.450.000",
                "Rp. 50.340.000",
                "Rp. 60.340.000",
                "Rp. 80.400.000",
                "Rp. 50.300.000",
                "Rp. 40.340.000",
                "Rp. 35.000.000",
                "Rp. 80.000.000",
                "Rp. 77.200.000",
                "Rp. 90.870.000",
                "Rp. 70.580.000",
                "Rp. 40.500.000",
                "Rp. 50.000.000",
                "Rp. 90.450.000",

        };
        return urls;
    }

    public static String[] getBuahDana() {
        String[] urls = new String[] {
                "Rp. 60.220.000",
                "Rp. 90.500.000",
                "Rp. 60.300.000",
                "Rp. 80.345.000",
                "Rp. 50.345.000",
                "Rp. 46.000.000",
                "Rp. 75.000.000",
                "Rp. 62.315.000",
                "Rp. 20.800.000",
                "Rp. 10.500.000",
                "Rp. 34.800.000",
                "Rp. 36.000.000",
                "Rp. 55.500.000",
                "Rp. 88.225.000",
        };
        return urls;
    }

    public static String[] getSerealiaDana() {
        String[] urls = new String[] {
                "Rp. 20.400.000",
                "Rp. 40.220.000",
                "Rp. 10.500.000",
                "Rp. 30.300.000",
                "Rp. 60.500.000",
                "Rp. 20.345.000",
                "Rp. 50.345.000",
                "Rp. 60.000.000",
                "Rp. 30.315.000",
                "Rp. 50.345.000",
                "Rp. 8.500.000",
                "Rp. 11.800.000",
                "Rp. 36.000.000",
                "Rp. 30.500.000",
                "Rp. 8.225.000",
                "Rp. 45.100.000",

        };
        return urls;
    }

    public static String[] getUbianDana() {
        String[] urls = new String[] {
                "Rp. 40.000.000",
                "Rp. 25.000.000",
                "Rp. 20.000.000",
                "Rp. 50.000.000",
                "Rp. 20.000.000",
                "Rp. 70.000.000",
                "Rp. 90.000.000",
                "Rp. 50.000.000",
                "Rp. 30.000.000",
                "Rp. 60.000.000",
                "Rp. 70.000.000",
                "Rp. 35.000.000",
                "Rp. 60.000.000",
                "Rp. 90.000.000",
                "Rp. 55.000.000",
                "Rp. 90.000.000",
                "Rp. 45.000.000",

        };
        return urls;
    }

    public static String[] getKacangDana() {
        String[] urls = new String[] {
                "Rp. 70.500.000",
                "Rp. 15.220.000",
                "Rp. 90.500.000",
                "Rp. 60.300.000",
                "Rp. 20.345.000",
                "Rp. 90.030.000",
                "Rp. 53.080.000",
                "Rp. 70.345.000",
                "Rp. 50.123.000",
                "Rp. 78.421.000",
                "Rp. 89.000.000",
                "Rp. 53.000.000",
                "Rp. 74.534.000",
                "Rp. 64.340.000",
                "Rp. 76.890.000",
                "Rp. 55.745.000",

        };
        return urls;
    }

    public static String[] getHomeDana() {
        String[] urls = new String[] {
                "Rp. 60.500.000",
                "Rp. 70.700.000",
                "Rp. 80.450.000",
                "Rp. 50.340.000",
                "Rp. 60.340.000",
                "Rp. 80.400.000",
                "Rp. 50.300.000",
                "Rp. 40.340.000",
                "Rp. 35.000.000",
                "Rp. 80.000.000",
                "Rp. 77.200.000",
                "Rp. 90.870.000",
                "Rp. 70.580.000",
                "Rp. 40.500.000",
                "Rp. 50.000.000",
                "Rp. 90.450.000",
                "Rp. 60.220.000",
                "Rp. 90.500.000",
                "Rp. 60.300.000",
                "Rp. 80.345.000",
                "Rp. 50.345.000",
                "Rp. 46.000.000",
                "Rp. 75.000.000",
                "Rp. 62.315.000",
                "Rp. 20.800.000",
                "Rp. 10.500.000",
                "Rp. 34.800.000",
                "Rp. 36.000.000",
                "Rp. 55.500.000",
                "Rp. 88.225.000",
                "Rp. 20.400.000",
                "Rp. 40.220.000",
                "Rp. 10.500.000",
                "Rp. 30.300.000",
                "Rp. 60.500.000",
                "Rp. 20.345.000",
                "Rp. 50.345.000",
                "Rp. 60.000.000",
                "Rp. 30.315.000",
                "Rp. 50.345.000",
                "Rp. 8.500.000",
                "Rp. 11.800.000",
                "Rp. 36.000.000",
                "Rp. 30.500.000",
                "Rp. 8.225.000",
                "Rp. 45.100.000",
                "Rp. 40.000.000",
                "Rp. 25.000.000",
                "Rp. 20.000.000",
                "Rp. 50.000.000",
                "Rp. 20.000.000",
                "Rp. 70.000.000",
                "Rp. 90.000.000",
                "Rp. 50.000.000",
                "Rp. 30.000.000",
                "Rp. 60.000.000",
                "Rp. 70.000.000",
                "Rp. 35.000.000",
                "Rp. 60.000.000",
                "Rp. 90.000.000",
                "Rp. 55.000.000",
                "Rp. 90.000.000",
                "Rp. 45.000.000",
                "Rp. 70.500.000",
                "Rp. 15.220.000",
                "Rp. 90.500.000",
                "Rp. 60.300.000",
                "Rp. 20.345.000",
                "Rp. 90.030.000",
                "Rp. 53.080.000",
                "Rp. 70.345.000",
                "Rp. 50.123.000",
                "Rp. 78.421.000",
                "Rp. 89.000.000",
                "Rp. 53.000.000",
                "Rp. 74.534.000",
                "Rp. 64.340.000",
                "Rp. 76.890.000",
                "Rp. 55.745.000",
        };
        return urls;
    }

    public static String[] getSayurDetail() {
        String[] urls = new String[] {
                "-\tSudah mulai bertani sejak tahun 2010\n" +
                        "-\tHasil panen tidak pernah rusak\n",
                "-\tSudah mulai bertani sejak 2007\n" +
                        "-\tSelain kubis, dia juga biasanya menanam brokoli,bayam dan sayuran hijau lainnya.\n",
                "-\tMulai bertani sejak tahun 2003\n" +
                        "-\tHasil panennya sangat berkualitas\n",
                "-\tSudah mulai bertani sejak tahun 2000. \n" +
                        "-\tSelain cabai, biasanya juga menanam sayuran hijau lainnya\n",
                "-\tMulai bertani sejak tahun 2003\n" +
                        "-\tMampu mengatur lahan dengan baik\n",
                "-\tMulai bertani sejak tahun 2009\n" +
                        "-\tHasil panen  bersaing di ibukota\n",
                "-\tMulai bertani sejak tahun 2000\n" +
                        "-\tMemiliki banyak anggota\n",
                "-\tMulai bertani sejak tahun 2010\n" +
                        "-\tSudah mendapatkan pelatihan\n",
                "-\tMulai bertani sejak tahun 2001\n" +
                        "-\tSejauh ini sudah beberapa investor yang mendanai\n",
                "-\tMulai bertani sejak tahun 1996",
                "-\tMulai bertani sejak tahun 2012",
                "-\tMulai bertani sejak tahun 2004",
                "-\tMulai bertani sejak tahun 2003\n" +
                        "-\tBaru saja berhasil memanen seluruh ladangnya\n",
                "-\tSudah 7 tahun dalam mengelola lahan\n" +
                        "-\tJurusan Agribisnis\n",
                "-\tMulai bertani pada tahun 2010\n" +
                        "-\tMemiliki beberapa kelompok dalam mengelola lahan\n",
                "-\tSudah 10 tahun dalam bertani",
        };
        return urls;
    }

    public static String[] getBuahDetail() {
        String[] urls = new String[] {
                "-\tMerupakan lahan yang dikelola turun temurun\n" +
                        "-\tSudah mulai bertani sejak kecil\n",
                "-\tSudah mulai bertani sejak tahun 2000\n" +
                        "-\tDiusianya 10 mulai bertani\n",
                "-\t Beberapa kali mendapatkan investor luar\n",
                "-\tPertama kali bertani sejak 8 tahun yang lalu\n" +
                        "-\tSebelum strawberry pernah menanam jeruk\n",
                "-\tSudah bertani sejak tahun 1998 .\n" +
                        "-\tMemiliki beberapa kelompok dalam mengelola lahan\n",
                "-\tMulai bertani sejak tahun 2004\n",
                "-\tSudah bertani sejak  tahun 2001\n" +
                        "-\tMemiliki banyak anggota\n",
                "-\tSudah bertani sejak  tahun 1980\n" +
                        "-\tHasil panennya sangat berkualitas\n",
                "-\tMulai bertani sejak tahun 1996\n",
                "-\tMulai bertani sejak tahun 2003\n" +
                        "-\tBaru saja berhasil memanen seluruh ladangnya\n",
                "-\tMulai bertani sejak tahun 2003\n" +
                        "-\tHasil panen  bersaing di ibukota\n",
                "-\tMemulai dari tahun 2000 \n" +
                        "-\tSudah menjual hasil panennya ke banyak kota besar\n",
                "-\tMulai bertani sejak tahun 1996\n" +
                        "-\tSudah mempunyai banyak investor asing\n",
                "-\tSudah mulai bertani sejak tahun 2005. -\t\n",
        };
        return urls;
    }

    public static String[] getSerealiaDetail() {
        String[] urls = new String[] {
                "-\tSudah mulai bertani sejak 1996\n" +
                        "-\tHasil panen tidak pernah rusak dan sudah dikenal warga lokal\n",
                "-\tMerupakan lahan yang dikelola turun temurun\n" +
                        "-\tSudah mulai bertani sejak kecil\n",
                "-\tSudah mulai bertani sejak tahun 2000\n" +
                        "-\tSelain padi dia juga menanam jagung\n",
                "-\tSudah bertani sejak tahun 1973\n" +
                        "-\tJagung yang dia tanam sudah dijual ke berbagai daerah\n",
                "-\tLahan yang dikelola merupakan usaha bersama dengan petani yang lain\n",
                "-\tSudah bertani sejak tahun 1984\n",
                "-\tSudah bertani sejak 1982\n" +
                        "-\tHasil panennya sudah dikenal warga lokal\n",
                "-\tSudah bertani sejak tahun 2002\n",
                "-\tSudah bertani sejak tahun 1980\n",
                "-\tSudah bertani sejak tahun 2001\n" +
                        "-\tMemiliki banyak anggota\n",
                "-\tBeberapa kali mendapatkan investor luar\n" +
                        "-\tHasil Panennya berkualitas tinggi\n",
                "-\tSudah bertani sejak tahun 2001\n",
                "-\tSudah bertani sejak tahun 1992\n",
                "-\tMemulai dari tahun 2000\n" +
                        "-\tSudah menjual hasil panennya ke banyak kota besar\n",
                "-\tMemiliki beberapa kelompok dalam mengelola lahan\n",
                "-\tSudah mulai bertani sejak tahun 1997\n" +
                        "-\tHasil panen tidak pernah rusak\n",
        };
        return urls;
    }

    public static String[] getUbianDetail() {
        String[] urls = new String[] {
                "-\tMerupakan lahan yang dikelola turun temurun\n" +
                        "-\tSudah mulai bertani sejak kecil\n",
                "-\tSudah 7 tahun dalam mengelola lahan",
                "-\tMerupakan lahan yang dikelola turun temurun",
                "-\tSudah mulai bertani sejak tahun 2010\n" +
                        "-\tHasil panen tidak pernah rusak\n",
                "-\tSudah 15 tahun dalam mengelola lahan",
                "-\tSudah 10 tahun dalam bertani",
                "-\tMerupakan lahan yang dikelola turun temurun",
                "-\tMerupakan lahan yang dikelola turun temurun\n" +
                        "-\tSudah mulai bertani sejak kecil\n",
                "-\tHasil panen merupakan kualitas unggul",
                "-\tSudah 10 tahun dalam bertani",
                "-\tSudah mulai bertani sejak tahun 2010\n" +
                        "-\tHasil panen tidak pernah rusak\n",
                "-\tHasil panen merupakan kualitas unggul",
                "-\tSejauh ini sudah beberapa investor yang mendanai",
                "-\tMerupakan lahan yang dikelola turun temurun\n" +
                        "-\tSudah mulai bertani sejak kecil\n",
                "-\tHasil panen tidak pernah rusak",
                "-\tSejauh ini sudah beberapa investor yang mendanai",
                "-\tHasil panen merupakan kualitas unggul",
        };
        return urls;
    }

    public static String[] getKacangDetail() {
        String[] urls = new String[] {
                "-\tSudah mulai bertani sejak tahun 2010\n" +
                        "-\tMemiliki beberapa kelompok tani di daerahnya\n",
                "-\tMerupakan lahan yang dikelola turun temurun\n" +
                        "-\tSudah mulai bertani sejak 2013\n",
                "-\tMemiliki beberapa lahan\n" +
                        "-\tDiusianya 13 tahun sudah mulai bertani\n",
                "-\tBeberapa kali mendapatkan investor luar\n" +
                        "-\tDimulai sejak 10 tahun yang lalu\n",
                "-\tPertama kali bertani sejak 5 tahun yang lalu\n" +
                        "-\tMemiliki kualitas tanam yang sangat baik\n",
                "-\tSudah mulai bertani sejak tahun 2003\n" +
                        "-\tLahan yang dikelolanya merupakan hasil turun temurun\n",
                "-\tSudah mulai bertani sejak kecil\n" +
                        "-\tSering mengikuti pelatihan-pelatihan yang berhubungan dengan pertanian\n",
                "-\tSudah mulai bertani sejak tahun 2000\n" +
                        "-\tDiusianya 10 mulai bertani\n",
                "-\tBeberapa kali mendapatkan investor luar\n" +
                        "-\tSudah mulai bertani sejak tahun 1999\n",
                "-\tPertama kali bertani sejak 8 tahun yang lalu\n" +
                        "-\tMemiliki pengalaman sebagai buruh tani di luar negeri\n",
                "-\tDiusianya 10 tahun mulai bertani\n" +
                        "-\tBeberapa kali mendapatkan investor luar\n",
                "-\tPertama kali bertani sejak 8 tahun yang lalu\n" +
                        "-\tDiusianya 11 mulai bertani\n",
                "-\tPertama kali bertani sejak 6 tahun yang lalu\n" +
                        "-\tMemiliki kualitas tanam yang sangat baik\n",
                "-\tSudah mulai bertani sejak tahun 2010\n" +
                        "-\tSudah beberapa kali mendapatkan investor\n",
                "-\tSering mendapatkan pelatihan\n" +
                        "-\tMulai bertani sejak 2010\n",
                "-\tSering mendapatkan pelatihan\n" +
                        "-\tMulai bertani sejak 2011\n",
        };
        return urls;
    }

    public static String[] getHomeDetail() {
        String[] urls = new String[] {
                "-\tSudah mulai bertani sejak tahun 2010\n" +
                        "-\tHasil panen tidak pernah rusak\n",
                "-\tSudah mulai bertani sejak 2007\n" +
                        "-\tSelain kubis, dia juga biasanya menanam brokoli,bayam dan sayuran hijau lainnya.\n",
                "-\tMulai bertani sejak tahun 2003\n" +
                        "-\tHasil panennya sangat berkualitas\n",
                "-\tSudah mulai bertani sejak tahun 2000. \n" +
                        "-\tSelain cabai, biasanya juga menanam sayuran hijau lainnya\n",
                "-\tMulai bertani sejak tahun 2003\n" +
                        "-\tMampu mengatur lahan dengan baik\n",
                "-\tMulai bertani sejak tahun 2009\n" +
                        "-\tHasil panen  bersaing di ibukota\n",
                "-\tMulai bertani sejak tahun 2000\n" +
                        "-\tMemiliki banyak anggota\n",
                "-\tMulai bertani sejak tahun 2010\n" +
                        "-\tSudah mendapatkan pelatihan\n",
                "-\tMulai bertani sejak tahun 2001\n" +
                        "-\tSejauh ini sudah beberapa investor yang mendanai\n",
                "-\tMulai bertani sejak tahun 1996",
                "-\tMulai bertani sejak tahun 2012",
                "-\tMulai bertani sejak tahun 2004",
                "-\tMulai bertani sejak tahun 2003\n" +
                        "-\tBaru saja berhasil memanen seluruh ladangnya\n",
                "-\tSudah 7 tahun dalam mengelola lahan\n" +
                        "-\tJurusan Agribisnis\n",
                "-\tMulai bertani pada tahun 2010\n" +
                        "-\tMemiliki beberapa kelompok dalam mengelola lahan\n",
                "-\tSudah 10 tahun dalam bertani",
                "-\tMerupakan lahan yang dikelola turun temurun\n" +
                        "-\tSudah mulai bertani sejak kecil\n",
                "-\tSudah mulai bertani sejak tahun 2000\n" +
                        "-\tDiusianya 10 mulai bertani\n",
                "-\t Beberapa kali mendapatkan investor luar\n",
                "-\tPertama kali bertani sejak 8 tahun yang lalu\n" +
                        "-\tSebelum strawberry pernah menanam jeruk\n",
                "-\tSudah bertani sejak tahun 1998 .\n" +
                        "-\tMemiliki beberapa kelompok dalam mengelola lahan\n",
                "-\tMulai bertani sejak tahun 2004\n",
                "-\tSudah bertani sejak  tahun 2001\n" +
                        "-\tMemiliki banyak anggota\n",
                "-\tSudah bertani sejak  tahun 1980\n" +
                        "-\tHasil panennya sangat berkualitas\n",
                "-\tMulai bertani sejak tahun 1996\n",
                "-\tMulai bertani sejak tahun 2003\n" +
                        "-\tBaru saja berhasil memanen seluruh ladangnya\n",
                "-\tMulai bertani sejak tahun 2003\n" +
                        "-\tHasil panen  bersaing di ibukota\n",
                "-\tMemulai dari tahun 2000 \n" +
                        "-\tSudah menjual hasil panennya ke banyak kota besar\n",
                "-\tMulai bertani sejak tahun 1996\n" +
                        "-\tSudah mempunyai banyak investor asing\n",
                "-\tSudah mulai bertani sejak tahun 2005. -\t\n",
                "-\tSudah mulai bertani sejak 1996\n" +
                        "-\tHasil panen tidak pernah rusak dan sudah dikenal warga lokal\n",
                "-\tMerupakan lahan yang dikelola turun temurun\n" +
                        "-\tSudah mulai bertani sejak kecil\n",
                "-\tSudah mulai bertani sejak tahun 2000\n" +
                        "-\tSelain padi dia juga menanam jagung\n",
                "-\tSudah bertani sejak tahun 1973\n" +
                        "-\tJagung yang dia tanam sudah dijual ke berbagai daerah\n",
                "-\tLahan yang dikelola merupakan usaha bersama dengan petani yang lain\n",
                "-\tSudah bertani sejak tahun 1984\n",
                "-\tSudah bertani sejak 1982\n" +
                        "-\tHasil panennya sudah dikenal warga lokal\n",
                "-\tSudah bertani sejak tahun 2002\n",
                "-\tSudah bertani sejak tahun 1980\n",
                "-\tSudah bertani sejak tahun 2001\n" +
                        "-\tMemiliki banyak anggota\n",
                "-\tBeberapa kali mendapatkan investor luar\n" +
                        "-\tHasil Panennya berkualitas tinggi\n",
                "-\tSudah bertani sejak tahun 2001\n",
                "-\tSudah bertani sejak tahun 1992\n",
                "-\tMemulai dari tahun 2000\n" +
                        "-\tSudah menjual hasil panennya ke banyak kota besar\n",
                "-\tMemiliki beberapa kelompok dalam mengelola lahan\n",
                "-\tSudah mulai bertani sejak tahun 1997\n" +
                        "-\tHasil panen tidak pernah rusak\n",
                "-\tMerupakan lahan yang dikelola turun temurun\n" +
                        "-\tSudah mulai bertani sejak kecil\n",
                "-\tSudah 7 tahun dalam mengelola lahan",
                "-\tMerupakan lahan yang dikelola turun temurun",
                "-\tSudah mulai bertani sejak tahun 2010\n" +
                        "-\tHasil panen tidak pernah rusak\n",
                "-\tSudah 15 tahun dalam mengelola lahan",
                "-\tSudah 10 tahun dalam bertani",
                "-\tMerupakan lahan yang dikelola turun temurun",
                "-\tMerupakan lahan yang dikelola turun temurun\n" +
                        "-\tSudah mulai bertani sejak kecil\n",
                "-\tHasil panen merupakan kualitas unggul",
                "-\tSudah 10 tahun dalam bertani",
                "-\tSudah mulai bertani sejak tahun 2010\n" +
                        "-\tHasil panen tidak pernah rusak\n",
                "-\tHasil panen merupakan kualitas unggul",
                "-\tSejauh ini sudah beberapa investor yang mendanai",
                "-\tMerupakan lahan yang dikelola turun temurun\n" +
                        "-\tSudah mulai bertani sejak kecil\n",
                "-\tHasil panen tidak pernah rusak",
                "-\tSejauh ini sudah beberapa investor yang mendanai",
                "-\tHasil panen merupakan kualitas unggul",
                "-\tSudah mulai bertani sejak tahun 2010\n" +
                        "-\tMemiliki beberapa kelompok tani di daerahnya\n",
                "-\tMerupakan lahan yang dikelola turun temurun\n" +
                        "-\tSudah mulai bertani sejak 2013\n",
                "-\tMemiliki beberapa lahan\n" +
                        "-\tDiusianya 13 tahun sudah mulai bertani\n",
                "-\tBeberapa kali mendapatkan investor luar\n" +
                        "-\tDimulai sejak 10 tahun yang lalu\n",
                "-\tPertama kali bertani sejak 5 tahun yang lalu\n" +
                        "-\tMemiliki kualitas tanam yang sangat baik\n",
                "-\tSudah mulai bertani sejak tahun 2003\n" +
                        "-\tLahan yang dikelolanya merupakan hasil turun temurun\n",
                "-\tSudah mulai bertani sejak kecil\n" +
                        "-\tSering mengikuti pelatihan-pelatihan yang berhubungan dengan pertanian\n",
                "-\tSudah mulai bertani sejak tahun 2000\n" +
                        "-\tDiusianya 10 mulai bertani\n",
                "-\tBeberapa kali mendapatkan investor luar\n" +
                        "-\tSudah mulai bertani sejak tahun 1999\n",
                "-\tPertama kali bertani sejak 8 tahun yang lalu\n" +
                        "-\tMemiliki pengalaman sebagai buruh tani di luar negeri\n",
                "-\tDiusianya 10 tahun mulai bertani\n" +
                        "-\tBeberapa kali mendapatkan investor luar\n",
                "-\tPertama kali bertani sejak 8 tahun yang lalu\n" +
                        "-\tDiusianya 11 mulai bertani\n",
                "-\tPertama kali bertani sejak 6 tahun yang lalu\n" +
                        "-\tMemiliki kualitas tanam yang sangat baik\n",
                "-\tSudah mulai bertani sejak tahun 2010\n" +
                        "-\tSudah beberapa kali mendapatkan investor\n",
                "-\tSering mendapatkan pelatihan\n" +
                        "-\tMulai bertani sejak 2010\n",
                "-\tSering mendapatkan pelatihan\n" +
                        "-\tMulai bertani sejak 2011\n",
        };
        return urls;
    }

    public static String[] getSayurLuas() {
        String[] urls = new String[] {
                "330mx250m",
                "400mx250m",
                "300mx760m",
                "350mx460m",
                "200mx560m",
                "400mx450m",
                "300mx250m",
                "200mx350m",
                "150mx200m",
                "500mx350m",
                "340mx278m",
                "500mx230m",
                "450mx300m",
                "350mx200m",
                "500mx320m",
                "540mx200m",
        };
        return urls;
    }

    public static String[] getBuahLuas() {
        String[] urls = new String[] {
                "400mx250m",
                "500mx320m",
                "350mx250m",
                "450mx250m",
                "200mx200m",
                "300mx200m",
                "200mx300m",
                "150mx150m",
                "400mx300m",
                "300mx200m",
                "250mx350m",
                "500mx500m",
                "350mx250m",
                "300mx300m",

        };
        return urls;
    }

    public static String[] getSerealiaLuas() {
        String[] urls = new String[] {
                "250mx250m",
                "550mx550m",
                "360mx250m",
                "210mx230m",
                "300mx300m",
                "250mx250m",
                "350mx350m",
                "200mx200m",
                "300mx250m",
                "200mx300m",
                "300mx300m",
                "120mx200m",
                "300mx250m",
                "200mx300m",
                "500mx200m",
                "200mx300m",

        };
        return urls;
    }

    public static String[] getUbianLuas() {
        String[] urls = new String[] {
                "300mx100m",
                "250mx250m",
                "100mx200m",
                "400mx100m",
                "100mx250m",
                "600mx400m",
                "800mx400m",
                "400mx100m",
                "200mx300m",
                "600mx200m",
                "500mx200m",
                "400mx400m",
                "500mx300m",
                "700mx200m",
                "300mx300m",
                "800mx400m",
                "600mx200m",
        };
        return urls;
    }

    public static String[] getKacangLuas() {
        String[] urls = new String[] {
                "250mx360m",
                "300mx250m",
                "700mx320m",
                "350mx250m",
                "450mx750m",
                "250mx360m",
                "600mx250m",
                "530mx320m",
                "340mx250m",
                "450mx250m",
                "220mx750m",
                "510mx310m",
                "330mx250m",
                "100mx850m",
                "740mx320m",
        };
        return urls;
    }

    public static String[] getHomeLuas() {
        String[] urls = new String[] {
                "330mx250m",
                "400mx250m",
                "300mx760m",
                "350mx460m",
                "200mx560m",
                "400mx450m",
                "300mx250m",
                "200mx350m",
                "150mx200m",
                "500mx350m",
                "340mx278m",
                "500mx230m",
                "450mx300m",
                "350mx200m",
                "500mx320m",
                "540mx200m",
                "400mx250m",
                "500mx320m",
                "350mx250m",
                "450mx250m",
                "200mx200m",
                "300mx200m",
                "200mx300m",
                "150mx150m",
                "400mx300m",
                "300mx200m",
                "250mx350m",
                "500mx500m",
                "350mx250m",
                "300mx300m",
                "250mx250m",
                "550mx550m",
                "360mx250m",
                "210mx230m",
                "300mx300m",
                "250mx250m",
                "350mx350m",
                "200mx200m",
                "300mx250m",
                "200mx300m",
                "300mx300m",
                "120mx200m",
                "300mx250m",
                "200mx300m",
                "500mx200m",
                "200mx300m",
                "300mx100m",
                "250mx250m",
                "100mx200m",
                "400mx100m",
                "100mx250m",
                "600mx400m",
                "800mx400m",
                "400mx100m",
                "200mx300m",
                "600mx200m",
                "500mx200m",
                "400mx400m",
                "500mx300m",
                "700mx200m",
                "300mx300m",
                "800mx400m",
                "600mx200m",
                "250mx360m",
                "300mx250m",
                "700mx320m",
                "350mx250m",
                "450mx750m",
                "250mx360m",
                "600mx250m",
                "530mx320m",
                "340mx250m",
                "450mx250m",
                "220mx750m",
                "510mx310m",
                "330mx250m",
                "100mx850m",
                "740mx320m",
        };
        return urls;
    }

    public static String[] getSayurLokasi() {
        String[] urls = new String[] {
                "Desa Awiluar",
                "Desa Babadan",
                "Desa Alahan Mti",
                "Desa Panisi",
                "Desa Panjang",
                "Desa Panobasan",
                "Pulau Ternate",
                "Desa Aketola",
                "Desa Adi Jaya",
                "Desa Bandar Jaya",
                "Bah Jami",
                "Desa Ambit",
                "Desa Amis Indramayu",
                "Desa Amal",
                "Amporiwo",
                "Desa Ayumolinggo",
        };
        return urls;
    }

    public static String[] getSayurAlamat() {
        String[] urls = new String[] {
                "Desa Awiluar, Ciamis",
                "Desa Babadan, Cirebon",
                "Desa Alahan Mti, Padang",
                "Desa Panisi, Toba Samosir",
                "Desa Panjang, Batu Bara",
                "Desa Panobasan, Tapanuli Selatan",
                "Pulau Ternate, Ternate",
                "Desa Aketola, Halhamera Barat",
                "Desa Adi Jaya, Tebo",
                "Desa Bandar Jaya, Bungo",
                "Bah Jami, Simalungun",
                "Desa Ambit, Sumedang",
                "Desa Amis Indramayu",
                "Desa Amal, Donggala",
                "Amporiwo, Poso",
                "Desa Ayumolinggo",
        };
        return urls;
    }

    public static String[] getBuahLokasi() {
        String[] urls = new String[] {
                "Desa Bonggo Dua",
                "Desa Adiarsa",
                "Aek Bolon Jae",
                "Bali Agung",
                "Desa Babakansari",
                "Desa Baribis",
                "Desa Ciranjeng",
                "Desa Babajurang",
                "Desa Burujul Kulon",
                "Desa Sunia",
                "Desa Silihwangi",
                "Desa Ciranjeng",
                "Desa Salawana",
                "Desa Cicadas",
        };
        return urls;
    }

    public static String[] getBuahAlamat() {
        String[] urls = new String[] {
                "Desa Bonggo Dua, Bungalemo",
                "Desa Adiarsa, Purbalingga",
                "Aek Bolon Jae, Toba Samosir",
                "Bali Agung, Lampung",
                "Desa Babakansari, Asahan",
                "Desa Baribis, Purbalingga",
                "Desa Ciranjeng, Batubara",
                "Desa Babajurang, Gido",
                "Desa Burujul Kulon, Lahomi",
                "Desa Sunia, Lotu",
                "Desa Silihwangi, Tarempa",
                "Desa Ciranjeng, Ranai",
                "Desa Salawana, Daik",
                "Desa Cicadas, Bintuhan",
        };
        return urls;
    }

    public static String[] getSerealiaLokasi() {
        String[] urls = new String[] {
                "Desa Krecek",
                "Desa Cigelam",
                "Desa Banyumanik",
                "Desa Citrajaya",
                "Desa Sirnagalih",
                "Desa Cibendung",
                "Desa Kaliwadas",
                "Desa Brangkal",
                "Desa Ciawilor",
                "Desa Arjawinangun",
                "Desa Dangiang",
                "Desa Cilendek",
                "Desa Sukamaju",
                "Desa Mulyasari",
                "Desa Gedawang",
                "Desa Pakelan",
        };
        return urls;
    }

    public static String[] getSerealiaAlamat() {
        String[] urls = new String[] {
                "Desa Krecek, Kediri",
                "Desa Cigelam, Purwakarta",
                "Desa Banyumanik, Semarang",
                "Desa Citrajaya, Subang",
                "Desa Sirnagalih, Bandung",
                "Desa Cibendung, Brebes",
                "Desa Kaliwadas, Tegal",
                "Desa Brangkal, Jombang",
                "Desa Ciawilor, Kuningan",
                "Desa Arjawinangun, Cirebon",
                "Desa Dangiang, Garut",
                "Desa Cilendek, Bogor",
                "Desa Sukamaju, Bandung",
                "Desa Mulyasari, Subang",
                "Desa Gedawang, Semarang",
                "Desa Pakelan, Kediri",
        };
        return urls;
    }

    public static String[] getUbianLokasi() {
        String[] urls = new String[] {
                "Air Joman",
                "Bangun Sari",
                "Gunung Maligas‎",
                "Greahan",
                "Huta Bayu Raja‎",
                "Dolok Batunanggar",
                "Suka Lewei",
                "Sinembah Tanjung",
                "Bandar Gunung",
                "Haranggaol Horison",
                "Silo Bonto",
                "Ajibata",
                "Balige",
                "Barusjahe",
                "Ujung Padang",
                "Semangat",
                "Tangkidik",
        };
        return urls;
    }

    public static String[] getUbianAlamat() {
        String[] urls = new String[] {
                "Air Joman, Asahan",
                "Bangun Sari, Silau Laut, Asahan",
                "Gunung Maligas, Simalungun‎",
                "Greahan, Bangun Purba, Deli Serdang",
                "Huta Bayu Raja, Simalungun‎",
                "Dolok Batunanggar, Simalungun",
                "Suka Lewei, Bangun Purba, Deli Serdang",
                "Sinembah Tanjung Muda Hilir, Deli Serdang",
                "Bandar Gunung, Bangun Purba, Deli Serdang",
                "Haranggaol Horison, Simalungun‎",
                "Silo Bonto, Silau Laut, Asahan",
                "Ajibata, Toba Samosir",
                "Balige, Toba Samosir",
                "Barusjahe, Barusjahe, Karo",
                "Ujung Padang, Simalungun",
                "Semangat, Barusjahe, Karo",
                "Tangkidik, Barusjahe, Karo",
        };
        return urls;
    }

    public static String[] getKacangLokasi() {
        String[] urls = new String[] {
                "Kubung",
                "Air dikit",
                "Bunga Raya",
                "Alafan",
                "Bandar Huluan",
                "Tanah Pinem",
                "SeinLapan",
                "Kabanjahe",
                "SeinDadap",
                "Pagindar",
                "Gido",
                "Pauh",
                "Belinyu",
                "Jarai",
                "Julin",
                "Meranti",
        };
        return urls;
    }

    public static String[] getKacangAlamat() {
        String[] urls = new String[] {
                "Kubung, Solok",
                "Air dikit, Muko-Muko",
                "Bunga Raya, Siak",
                "Alafan, Simeuleu",
                "Bandar Huluan, Simalungun",
                "Tanah Pinem, Dairi",
                "SeiLapan, Langkat",
                "Kabanjahe, Karo",
                "SeiDadap, Asahan",
                "Pagindar, Pakpak Bharat",
                "Gido, Nias",
                "Pauh, Sarolangun",
                "Belinyu, Bangka",
                "Jarai, Lahat",
                "Julin, Bireuen",
                "Meranti, Asahan",

        };
        return urls;
    }

    public static String[] getHomeLokasi() {
        String[] urls = new String[] {
                "Desa Awiluar",
                "Desa Babadan",
                "Desa Alahan Mti",
                "Desa Panisi",
                "Desa Panjang",
                "Desa Panobasan",
                "Pulau Ternate",
                "Desa Aketola",
                "Desa Adi Jaya",
                "Desa Bandar Jaya",
                "Bah Jami",
                "Desa Ambit",
                "Desa Amis Indramayu",
                "Desa Amal",
                "Amporiwo",
                "Desa Ayumolinggo",
                "Desa Bonggo Dua",
                "Desa Adiarsa",
                "Aek Bolon Jae",
                "Bali Agung",
                "Desa Babakansari",
                "Desa Baribis",
                "Desa Ciranjeng",
                "Desa Babajurang",
                "Desa Burujul Kulon",
                "Desa Sunia",
                "Desa Silihwangi",
                "Desa Ciranjeng",
                "Desa Salawana",
                "Desa Cicadas",
                "Desa Krecek",
                "Desa Cigelam",
                "Desa Banyumanik",
                "Desa Citrajaya",
                "Desa Sirnagalih",
                "Desa Cibendung",
                "Desa Kaliwadas",
                "Desa Brangkal",
                "Desa Ciawilor",
                "Desa Arjawinangun",
                "Desa Dangiang",
                "Desa Cilendek",
                "Desa Sukamaju",
                "Desa Mulyasari",
                "Desa Gedawang",
                "Desa Pakelan",
                "Air Joman",
                "Bangun Sari",
                "Gunung Maligas‎",
                "Greahan",
                "Huta Bayu Raja‎",
                "Dolok Batunanggar",
                "Suka Lewei",
                "Sinembah Tanjung",
                "Bandar Gunung",
                "Haranggaol Horison",
                "Silo Bonto",
                "Ajibata",
                "Balige",
                "Barusjahe",
                "Ujung Padang",
                "Semangat",
                "Tangkidik",
                "Kubung",
                "Air dikit",
                "Bunga Raya",
                "Alafan",
                "Bandar Huluan",
                "Tanah Pinem",
                "SeinLapan",
                "Kabanjahe",
                "SeinDadap",
                "Pagindar",
                "Gido",
                "Pauh",
                "Belinyu",
                "Jarai",
                "Julin",
                "Meranti",
        };
        return urls;
    }

    public static String[] getHomeAlamat() {
        String[] urls = new String[] {
                "Desa Awiluar, Ciamis",
                "Desa Babadan, Cirebon",
                "Desa Alahan Mti, Padang",
                "Desa Panisi, Toba Samosir",
                "Desa Panjang, Batu Bara",
                "Desa Panobasan, Tapanuli Selatan",
                "Pulau Ternate, Ternate",
                "Desa Aketola, Halhamera Barat",
                "Desa Adi Jaya, Tebo",
                "Desa Bandar Jaya, Bungo",
                "Bah Jami, Simalungun",
                "Desa Ambit, Sumedang",
                "Desa Amis Indramayu",
                "Desa Amal, Donggala",
                "Amporiwo, Poso",
                "Desa Ayumolinggo",
                "Desa Bonggo Dua, Bungalemo",
                "Desa Adiarsa, Purbalingga",
                "Aek Bolon Jae, Toba Samosir",
                "Bali Agung, Lampung",
                "Desa Babakansari, Asahan",
                "Desa Baribis, Purbalingga",
                "Desa Ciranjeng, Batubara",
                "Desa Babajurang, Gido",
                "Desa Burujul Kulon, Lahomi",
                "Desa Sunia, Lotu",
                "Desa Silihwangi, Tarempa",
                "Desa Ciranjeng, Ranai",
                "Desa Salawana, Daik",
                "Desa Cicadas, Bintuhan",
                "Desa Krecek, Kediri",
                "Desa Cigelam, Purwakarta",
                "Desa Banyumanik, Semarang",
                "Desa Citrajaya, Subang",
                "Desa Sirnagalih, Bandung",
                "Desa Cibendung, Brebes",
                "Desa Kaliwadas, Tegal",
                "Desa Brangkal, Jombang",
                "Desa Ciawilor, Kuningan",
                "Desa Arjawinangun, Cirebon",
                "Desa Dangiang, Garut",
                "Desa Cilendek, Bogor",
                "Desa Sukamaju, Bandung",
                "Desa Mulyasari, Subang",
                "Desa Gedawang, Semarang",
                "Desa Pakelan, Kediri",
                "Air Joman, Asahan",
                "Bangun Sari, Silau Laut, Asahan",
                "Gunung Maligas, Simalungun‎",
                "Greahan, Bangun Purba, Deli Serdang",
                "Huta Bayu Raja, Simalungun‎",
                "Dolok Batunanggar, Simalungun",
                "Suka Lewei, Bangun Purba, Deli Serdang",
                "Sinembah Tanjung Muda Hilir, Deli Serdang",
                "Bandar Gunung, Bangun Purba, Deli Serdang",
                "Haranggaol Horison, Simalungun‎",
                "Silo Bonto, Silau Laut, Asahan",
                "Ajibata, Toba Samosir",
                "Balige, Toba Samosir",
                "Barusjahe, Barusjahe, Karo",
                "Ujung Padang, Simalungun",
                "Semangat, Barusjahe, Karo",
                "Tangkidik, Barusjahe, Karo",
                "Kubung, Solok",
                "Air dikit, Muko-Muko",
                "Bunga Raya, Siak",
                "Alafan, Simeuleu",
                "Bandar Huluan, Simalungun",
                "Tanah Pinem, Dairi",
                "SeiLapan, Langkat",
                "Kabanjahe, Karo",
                "SeiDadap, Asahan",
                "Pagindar, Pakpak Bharat",
                "Gido, Nias",
                "Pauh, Sarolangun",
                "Belinyu, Bangka",
                "Jarai, Lahat",
                "Julin, Bireuen",
                "Meranti, Asahan",
        };
        return urls;
    }

    public static String[] getSayurNama() {
        String[] urls = new String[] {
                "Sri Wahyuni",
                "Kuncoro",
                "Agus Mulyadi",
                "Ratna Ginting",
                "Bobby Bahrun",
                "Arif Rahman",
                "Desi Jahana",
                "Boy Garahi",
                "Sadoria",
                "Erna Lugis",
                "Anggita",
                "Maysyarah",
                "Nurhasanah",
                "Hasanudin Mahar",
                "Said Alwi",
                "Rinaldi Septiadi",

        };
        return urls;
    }

    public static String[] getBuahNama() {
        String[] urls = new String[] {
                "Rahmad Farul",
                "Bagus Rahario",
                "Joshua Himala",
                "Felix Damil",
                "Widoyo",
                "Panji Laksana",
                "Ahmad Jonny",
                "Eko Widyanto",
                "Deni Hermawan",
                "Rahmad Aditya",
                "Nugroho Setiawan",
                "Riski Hidayat",
                "Widya Putri",
                "Lia Kartika",
        };
        return urls;
    }
    public static String[] getSerealiaNama() {
        String[] urls = new String[] {
                "Toni Wijaksono",
                "Hadi Wijaya",
                "Muhamad Bayu",
                "Surya Prima",
                "Risna Sari",
                "Riska Febriyani",
                "Tika Lestari",
                "Rina Sari",
                "Dian Febriyanti",
                "Sri Lestari",
                "Agus Suryadi",
                "Fatimah",
                "Surya Rahmat",
                "Andy Sutoyo",
                "Nur Halimah",
                "Abdul Hidayat",
        };
        return urls;
    }
    public static String[] getUbianNama() {
        String[] urls = new String[] {
                "Sri Wedari",
                "Ilham Suhari",
                "Agussuparjo",
                "Sutanto",
                "Rina Ginting",
                "Darwin sirait",
                "Sulaiman",
                "H. Arsyad",
                "Tina Samosir",
                "Heri Suyadi",
                "Jaya sihotang",
                "Johanes parlindungan",
                "Sri Utami",
                "Ahmad Wijaya",
                "Adam Nasution",
                "Riski Suyanto",
                "Haryono",
        };
        return urls;
    }

    public static String[] getKacangNama() {
        String[] urls = new String[] {
                "Farul Hanafi",
                "Rinaldi Hasan",
                "Dimas Agung",
                "Sri Hati",
                "Andre Sianipar",
                "Hadi Yunus",
                "Sari Namira",
                "Tri Husna",
                "Hartono",
                "Hartanti",
                "Dwi Cahya",
                "Putra",
                "Arya",
                "Wira",
                "Hartati",
        };
        return urls;
    }

    public static String[] getHomeNama() {
        String[] urls = new String[] {
                "Sri Wahyuni",
                "Kuncoro",
                "Agus Mulyadi",
                "Ratna Ginting",
                "Bobby Bahrun",
                "Arif Rahman",
                "Desi Jahana",
                "Boy Garahi",
                "Sadoria",
                "Erna Lugis",
                "Anggita",
                "Maysyarah",
                "Nurhasanah",
                "Hasanudin Mahar",
                "Said Alwi",
                "Rinaldi Septiadi",
                    "Rahmad Farul",
                    "Bagus Rahario",
                    "Joshua Himala",
                    "Felix Damil",
                    "Widoyo",
                    "Panji Laksana",
                    "Ahmad Jonny",
                    "Eko Widyanto",
                    "Deni Hermawan",
                    "Rahmad Aditya",
                    "Nugroho Setiawan",
                    "Riski Hidayat",
                    "Widya Putri",
                    "Lia Kartika",
                    "Toni Wijaksono",
                    "Hadi Wijaya",
                    "Muhamad Bayu",
                    "Surya Prima",
                    "Risna Sari",
                    "Riska Febriyani",
                    "Tika Lestari",
                    "Rina Sari",
                    "Dian Febriyanti",
                    "Sri Lestari",
                    "Agus Suryadi",
                    "Fatimah",
                    "Surya Rahmat",
                    "Andy Sutoyo",
                    "Nur Halimah",
                    "Abdul Hidayat",
                    "Sri Wedari",
                    "Ilham Suhari",
                    "Agussuparjo",
                    "Sutanto",
                    "Rina Ginting",
                    "Darwin sirait",
                    "Sulaiman",
                    "H. Arsyad",
                    "Tina Samosir",
                    "Heri Suyadi",
                    "Jaya sihotang",
                    "Johanes parlindungan",
                    "Sri Utami",
                    "Ahmad Wijaya",
                    "Adam Nasution",
                    "Riski Suyanto",
                    "Haryono",
                    "Farul Hanafi",
                    "Rinaldi Hasan",
                    "Dimas Agung",
                    "Sri Hati",
                    "Andre Sianipar",
                    "Hadi Yunus",
                    "Sari Namira",
                    "Tri Husna",
                    "Hartono",
                    "Hartanti",
                    "Dwi Cahya",
                    "Putra",
                    "Arya",
                    "Wira",
                    "Hartati",
        };
        return urls;
    }

    public static String[] getHomeUrls() {
        String[] urls = new String[]{
                "https://www.maxmanroe.com/wp-content/uploads/2014/12/Aplikasi-Petani-8villages.jpg",
                "https://statik.tempo.co/data/2008/11/16/id_3764/3764_620.jpg",
                "https://www.pertanianku.com/wp-content/uploads/2017/08/Petani-Ini-Sukses-Ubah-Perkebunan-Sawit-Jadi-Ladang-Sayuran-Beromzet-Ratusan-Juta.jpg",
                "https://surabayapost.net/wp-content/themes/sbypos/timthumb.php?src=https://surabayapost.net/wp-content/uploads/2017/11/IMG-20171114-WA0073.jpg&w=847&q=100",
                "http://krjogja.com/kr-admin//files/news/image/38544/Sayur%20Dasih.jpg",
                "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRSkwKDQX07ww6SeyiawP8HhbUf-uFzvWtg4w5N_DJFl947sug1",
                "http://cdn0-a.production.images.static6.com/QKhmdD66DUclHLOZUX7FVo41Wws=/673x379/smart/filters:quality(75):strip_icc():format(jpeg)/liputan6-media-production/medias/749673/original/071625100_1413004789-Petani_sayur_di_Palu.jpg",
                "http://www.medanbisnisdaily.com/imagesfile/201312/20131217075351_684.gif",
                "https://www.posbali.id/wp-content/uploads/2016/08/Salah-seorang-petani-ketika-memelihara-tanaman-sayur-hijau-di-sawahnya.jpg",
                "http://cdn2.tstatic.net/tribunnews/foto/bank/images/20140206_112452_nur-yasin-petani-organik.jpg",
                "https://ecs7.tokopedia.net/img/cache/700/product-1/2017/2/14/122987896/122987896_9ae3d1ec-e791-490b-b3b2-5da839c3a72d_448_300.jpg",
                "http://1.bp.blogspot.com/-7EsE7r33ijk/VdUIXYi3tBI/AAAAAAAAMaU/PgLKRoiHFvw/s400/sayuran.JPG",
                "http://www.pikiran-rakyat.com/sites/files/public/image/2011/04/23/panen_terong_ungu2.jpg",
                "https://cdns.klimg.com/merdeka.com/i/w/news/2014/02/06/317298/670x335/petani-di-gunung-slamet-pasrah-banyak-tanaman-sayuran-membusuk.jpg",
                "http://www.timormedia.com/images/SAYUR.jpg",
                "http://cdn2.tstatic.net/batam/foto/bank/images/riyanto-34-petani-jagung-kampung-wacopek-bintan_20150629_180248.jpg",
                "http://images1.rri.co.id/thumbs/berita_336524_800x600_buah_naga.jpg",
                "http://www.sumut24.co/wp-content/uploads/2016/02/petani-buah.jpg",
                "http://www.mongabay.co.id/wp-content/uploads/2016/02/kakao3-Petani-di-Kecamatan-Wotu-memilih-buah-kakao-yang-sehat-dari-hasil-binaan-PT-Mars-FOTO-OLEH-IQBAL-LUBIS.jpg",
                "http://ocmcdn.oomph.co.id/uploads/shareit_contents/2017/11/03/28681/8_20171103022643_petanistrawberi.jpg",
                "http://st294294.sitekno.com/images/art_43645.jpg",
                "http://v-images2.antarafoto.com/petani-blewah-m6tu97-prv.jpg",
                "https://static.simomot.com/wp-content/uploads/2015/01/antarafoto-panen-labu-220115-da.jpg",
                "https://bulelengkab.go.id/assets/images/berita/Gerokgak-Pusat-Budi-Daya-Pisang-Ijo_092326.jpg",
                "http://hariansib.co/photo/dir022016/hariansib_Hama-Lalat-Buah-Mengganas--Petani-Jeruk-di-Dairi-Terancam-Gagal-Panen.jpg",
                "https://tabloidsergap.files.wordpress.com/2013/05/apel-kota-batu.jpg",
                "http://beritadaerah.co.id/wp-content/uploads/2014/10/Panen-Pepaya-221014-RM-3.jpg",
                "https://khazanahleuser.files.wordpress.com/2013/03/sam_02291.jpg?w=500",
                "http://www.telegraph.co.uk/content/dam/food-and-drink/2015/11/19/82252078_F04AJF_Nasik_Maharashtra_India_27th_Apr_2013_27_April_2013__Nasik_INDIAAt_a_vineyard_a_Grap-xlarge_trans_NvBQzQNjv4BqZgEkZX3M936N5BQK4Va8RWtT0gK_6EfZT336f62EI5U.jpg",
                "http://1.bp.blogspot.com/-SV6MyI3fpZA/VQ9-NSQFNNI/AAAAAAAAE_w/FhKBl3ptqJw/s1600/Hama%2Bdan%2BPenyakit%2BStroberi.png",
                "http://4.bp.blogspot.com/-pFmSJPtZbQc/Ula895oeokI/AAAAAAAAAY4/gfxcfOZ_Klw/s1600/43Pertanian-India.jpg",
                "http://images.solopos.com/2016/03/sorgum.jpg",
                "https://bregadiumcianjur.files.wordpress.com/2011/01/bpk-asmudi-2_demak.jpg",
                "https://duniatraining.com/wp-content/uploads/2013/01/PetaniJagung.jpg",
                "https://2.bp.blogspot.com/-AwN4lTPttPs/VQ0UsCUPuYI/AAAAAAAAbc0/-T4crmbH0ig/s1600/desa-b.jpg",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/0/07/Rice_02.jpg/1024px-Rice_02.jpg",
                "http://www.unmultimedia.org/radio/english/wp-content/uploads/2017/01/325558-rice-625-415.jpg",
                "https://cdns.klimg.com/newshub.id/news/2017/02/17/120214/musim-tanam-ini-petani-padi-kudus-rugi-besar-170217j.jpg",
                "http://poskotanews.com/cms/wp-content/uploads/2017/05/petani-memanen-padi.jpg",
                "https://www.jawapos.com/radar/uploads/radarbromo/news/2017/10/25/padi-kentang-sumbang-pad-paling-tinggi-dari-pertanian_m_22345.jpeg",
                "https://kabartani.com/wp-content/uploads/2017/03/Tradisi-Wiwitan-Upacara-Panen-Padi-Bantul-Kabartani-640x359.jpg",
                "https://s0.bukalapak.com/img/531192844/m-1000-1000/Paket_Pupuk_Organik_Untuk_Budidaya_Tanaman_Jagung.jpg",
                "https://statik.tempo.co/data/2014/11/26/id_347091/347091_620.jpg",
                "https://kabartani.com/wp-content/uploads/2017/01/Petani-padi-melakukan-penyemprotan-pestisida-akibat-serangan-wereng-Kabartani-640x339.jpg",
                "https://cdn.pixabay.com/photo/2015/07/29/19/43/farmer-866487_960_720.jpg",
                "http://sumaterapost.co/wp-content/uploads/ktz/IMG_20171215_833-357kqapoosaoaslu0b7ua2.jpg",
                "http://kabarhandayani.com/wp-content/uploads/2015/07/gaplek.jpg",
                "http://litbang.kemendagri.go.id/website/wp-content/uploads/2017/09/Ubi-Kayu-Pandanwangi-Soko.gif",
                "http://cdn2.tstatic.net/tribunnews/foto/images/preview/petani-ubi-cilembu-panen-di-kampung-cikoneng_20160816_151053.jpg",
                "http://www.indonesiabiru.com/wp-content/uploads/2016/08/Petani-Kentang-Ranu-Pani.jpg",
                "http://ayobandung.com/images-bandung/post/photos/2017/12/28/4702/petani-kentang----dani-8.jpeg",
                "https://i2.wp.com/semarak.news/wp-content/uploads/2017/02/ptanidieng.png?fit=472%2C446&ssl=1",
                "http://sahabatpetani.com/wp-content/uploads/2017/11/foto-rachmad-yogi.rev_-715x400.jpg",
                "http://i2.wp.com/homestaysikunir.com/wp-content/uploads/2015/10/kentang-sikunir.jpg?resize=350%2C200",
                "http://cdn.jitunews.com/dynamic/article/2015/07/10/17422/CZuFuAWU09.jpg?w=630",
                "http://cdn2.tstatic.net/palembang/foto/bank/images/darman-menunjukkan-contoh-umbi-ubi-racun.jpg",
                "http://cdn2.tstatic.net/kupang/foto/bank/images/yohanes-hale-menunjukkan-tanaman-maek-bako_20170125_220355.jpg",
                "http://2.bp.blogspot.com/-8RSONkExEbM/VSSxN0me-PI/AAAAAAAAAsk/2zYM9gK0jcs/s1600/petani-singkong-sejarah-keripik-singkong-bapake.jpg",
                "http://cdn-tin.timestechnet.com/images/2016/09/01/Petani-singkongmhzBJ.jpg",
                "https://i0.wp.com/1.bp.blogspot.com/-bc_XKL4ls-c/VRKAtuQqUgI/AAAAAAAABuY/p8aXuw4cjys/s1600/DSC01197.JPG",
                "http://cdn2.tstatic.net/bangka/foto/bank/images/budi-merawat-ubi-kasesa_20160502_140311.jpg",
                "http://hargabarangterbaru.top/wp-content/uploads/2017/01/Pembeli-dan-Penjual-UDANG-ubi.jpg",
                "https://husnulkh.files.wordpress.com/2011/12/cimg0900.jpg",
                "https://1.bp.blogspot.com/-JTKwJTakTbg/WaUtCAqrf7I/AAAAAAAABXs/MBHk4Mr0ww8jg0kEs0-XM4N0PMqnvO_3wCLcBGAs/s1600/bawah%2BHL.jpg",
                "http://krjogja.com/kr-admin//files/news/image/43451/kacang%20tanah%20panen.jpg",
                "https://www.dictio.id/uploads/db3342/original/2X/c/c7ef4f54c350163d0efe05c22560975ddf2d173a.png",
                "http://v-images2.antarafoto.com/petani-binaan-hortindo-ne7hup-prv.jpg",
                "http://agrokomplekskita.com/wp-content/uploads/2016/12/petani-kacang-tanah.jpg",
                "http://krjogja.com/kr-admin//files/news/images/11208/Andele1.jpg",
                "http://beritadaerah.co.id/wp-content/uploads/2013/10/kacang-tanah.jpeg",
                "https://4.bp.blogspot.com/-ZoLRUVl48bY/WPwpVGwfCeI/AAAAAAAAD_M/UVKaGz2GCzMrxrNqH_PnnWYyWA1_kHh7ACLcB/s640/PETANI%2BDESA%2BBANARAN%2B%25285%2529.jpg",
                "http://4.bp.blogspot.com/-o7XIsQ_Bcrg/VMj6nvGyMNI/AAAAAAAAChs/bIaefx07P7Y/s1600/Petani%2BDesa%2BTempuran%2BBlora%2BGalau%2BKarena%2BHarga%2BKacang%2BPanjang%2BTerjun%2BBebas.jpg",
                "http://cdn2.tstatic.net/jogja/foto/bank/images/panen-kacang_2205.jpg",
                "https://store.tempo.co/cover/medium/foto/2012/01/02/s_AAN2011123003.jpg/",
                "http://www.medanbisnisdaily.com/imagesfile/201401/20140125090728_840.gif",
                "http://2.bp.blogspot.com/-rFyVqzutoUM/U-Bs-qC7jSI/AAAAAAAAFl8/BHIg_DKHpeA/s1600/llk.jpg",
                "http://assets.akurat.co/images/uploads/akurat_20170921031734_s5KP01.jpg",
                "http://www.koranmadura.com/wp-content/uploads/2015/08/petani.jpg",
        };
        return urls;
    }
    public static String[] getBuahUrls() {
        String[] urls = new String[]{
                "http://images1.rri.co.id/thumbs/berita_336524_800x600_buah_naga.jpg",
                "http://www.sumut24.co/wp-content/uploads/2016/02/petani-buah.jpg",
                "http://www.mongabay.co.id/wp-content/uploads/2016/02/kakao3-Petani-di-Kecamatan-Wotu-memilih-buah-kakao-yang-sehat-dari-hasil-binaan-PT-Mars-FOTO-OLEH-IQBAL-LUBIS.jpg",
                "http://ocmcdn.oomph.co.id/uploads/shareit_contents/2017/11/03/28681/8_20171103022643_petanistrawberi.jpg",
                "http://st294294.sitekno.com/images/art_43645.jpg",
                "http://v-images2.antarafoto.com/petani-blewah-m6tu97-prv.jpg",
                "https://static.simomot.com/wp-content/uploads/2015/01/antarafoto-panen-labu-220115-da.jpg",
                "https://bulelengkab.go.id/assets/images/berita/Gerokgak-Pusat-Budi-Daya-Pisang-Ijo_092326.jpg",
                "http://hariansib.co/photo/dir022016/hariansib_Hama-Lalat-Buah-Mengganas--Petani-Jeruk-di-Dairi-Terancam-Gagal-Panen.jpg",
                "https://tabloidsergap.files.wordpress.com/2013/05/apel-kota-batu.jpg",
                "http://beritadaerah.co.id/wp-content/uploads/2014/10/Panen-Pepaya-221014-RM-3.jpg",
                "https://khazanahleuser.files.wordpress.com/2013/03/sam_02291.jpg?w=500",
                "http://www.telegraph.co.uk/content/dam/food-and-drink/2015/11/19/82252078_F04AJF_Nasik_Maharashtra_India_27th_Apr_2013_27_April_2013__Nasik_INDIAAt_a_vineyard_a_Grap-xlarge_trans_NvBQzQNjv4BqZgEkZX3M936N5BQK4Va8RWtT0gK_6EfZT336f62EI5U.jpg",
                "http://1.bp.blogspot.com/-SV6MyI3fpZA/VQ9-NSQFNNI/AAAAAAAAE_w/FhKBl3ptqJw/s1600/Hama%2Bdan%2BPenyakit%2BStroberi.png"
        };
        return urls;
    }

    public static String[] getSayurUrls() {
        String[] urls = new String[]{
                "https://www.maxmanroe.com/wp-content/uploads/2014/12/Aplikasi-Petani-8villages.jpg",
                "https://statik.tempo.co/data/2008/11/16/id_3764/3764_620.jpg",
                "https://www.pertanianku.com/wp-content/uploads/2017/08/Petani-Ini-Sukses-Ubah-Perkebunan-Sawit-Jadi-Ladang-Sayuran-Beromzet-Ratusan-Juta.jpg",
                "https://surabayapost.net/wp-content/themes/sbypos/timthumb.php?src=https://surabayapost.net/wp-content/uploads/2017/11/IMG-20171114-WA0073.jpg&w=847&q=100",
                "http://krjogja.com/kr-admin//files/news/image/38544/Sayur%20Dasih.jpg",
                "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRSkwKDQX07ww6SeyiawP8HhbUf-uFzvWtg4w5N_DJFl947sug1",
                "http://cdn0-a.production.images.static6.com/QKhmdD66DUclHLOZUX7FVo41Wws=/673x379/smart/filters:quality(75):strip_icc():format(jpeg)/liputan6-media-production/medias/749673/original/071625100_1413004789-Petani_sayur_di_Palu.jpg",
                "http://www.medanbisnisdaily.com/imagesfile/201312/20131217075351_684.gif",
                "https://www.posbali.id/wp-content/uploads/2016/08/Salah-seorang-petani-ketika-memelihara-tanaman-sayur-hijau-di-sawahnya.jpg",
                "http://cdn2.tstatic.net/tribunnews/foto/bank/images/20140206_112452_nur-yasin-petani-organik.jpg",
                "https://ecs7.tokopedia.net/img/cache/700/product-1/2017/2/14/122987896/122987896_9ae3d1ec-e791-490b-b3b2-5da839c3a72d_448_300.jpg",
                "http://1.bp.blogspot.com/-7EsE7r33ijk/VdUIXYi3tBI/AAAAAAAAMaU/PgLKRoiHFvw/s400/sayuran.JPG",
                "http://www.pikiran-rakyat.com/sites/files/public/image/2011/04/23/panen_terong_ungu2.jpg",
                "https://cdns.klimg.com/merdeka.com/i/w/news/2014/02/06/317298/670x335/petani-di-gunung-slamet-pasrah-banyak-tanaman-sayuran-membusuk.jpg",
                "http://www.timormedia.com/images/SAYUR.jpg",
                "http://cdn2.tstatic.net/batam/foto/bank/images/riyanto-34-petani-jagung-kampung-wacopek-bintan_20150629_180248.jpg"
        };
        return urls;
    }

    public static String[] getSerealiaUrls() {
        String[] urls = new String[]{
                "http://4.bp.blogspot.com/-pFmSJPtZbQc/Ula895oeokI/AAAAAAAAAY4/gfxcfOZ_Klw/s1600/43Pertanian-India.jpg",
                "http://images.solopos.com/2016/03/sorgum.jpg",
                "https://bregadiumcianjur.files.wordpress.com/2011/01/bpk-asmudi-2_demak.jpg",
                "https://duniatraining.com/wp-content/uploads/2013/01/PetaniJagung.jpg",
                "https://2.bp.blogspot.com/-AwN4lTPttPs/VQ0UsCUPuYI/AAAAAAAAbc0/-T4crmbH0ig/s1600/desa-b.jpg",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/0/07/Rice_02.jpg/1024px-Rice_02.jpg",
                "http://www.unmultimedia.org/radio/english/wp-content/uploads/2017/01/325558-rice-625-415.jpg",
                "https://cdns.klimg.com/newshub.id/news/2017/02/17/120214/musim-tanam-ini-petani-padi-kudus-rugi-besar-170217j.jpg",
                "http://poskotanews.com/cms/wp-content/uploads/2017/05/petani-memanen-padi.jpg",
                "https://www.jawapos.com/radar/uploads/radarbromo/news/2017/10/25/padi-kentang-sumbang-pad-paling-tinggi-dari-pertanian_m_22345.jpeg",
                "https://kabartani.com/wp-content/uploads/2017/03/Tradisi-Wiwitan-Upacara-Panen-Padi-Bantul-Kabartani-640x359.jpg",
                "https://s0.bukalapak.com/img/531192844/m-1000-1000/Paket_Pupuk_Organik_Untuk_Budidaya_Tanaman_Jagung.jpg",
                "https://statik.tempo.co/data/2014/11/26/id_347091/347091_620.jpg",
                "https://kabartani.com/wp-content/uploads/2017/01/Petani-padi-melakukan-penyemprotan-pestisida-akibat-serangan-wereng-Kabartani-640x339.jpg",
                "https://cdn.pixabay.com/photo/2015/07/29/19/43/farmer-866487_960_720.jpg",
                "http://sumaterapost.co/wp-content/uploads/ktz/IMG_20171215_833-357kqapoosaoaslu0b7ua2.jpg"
        };
        return urls;
    }

    public static String[] getUbianUrls() {
        String[] urls = new String[]{
                "http://kabarhandayani.com/wp-content/uploads/2015/07/gaplek.jpg",
                "http://litbang.kemendagri.go.id/website/wp-content/uploads/2017/09/Ubi-Kayu-Pandanwangi-Soko.gif",
                "http://cdn2.tstatic.net/tribunnews/foto/images/preview/petani-ubi-cilembu-panen-di-kampung-cikoneng_20160816_151053.jpg",
                "http://www.indonesiabiru.com/wp-content/uploads/2016/08/Petani-Kentang-Ranu-Pani.jpg",
                "http://ayobandung.com/images-bandung/post/photos/2017/12/28/4702/petani-kentang----dani-8.jpeg",
                "https://i2.wp.com/semarak.news/wp-content/uploads/2017/02/ptanidieng.png?fit=472%2C446&ssl=1",
                "http://sahabatpetani.com/wp-content/uploads/2017/11/foto-rachmad-yogi.rev_-715x400.jpg",
                "http://i2.wp.com/homestaysikunir.com/wp-content/uploads/2015/10/kentang-sikunir.jpg?resize=350%2C200",
                "http://cdn.jitunews.com/dynamic/article/2015/07/10/17422/CZuFuAWU09.jpg?w=630",
                "http://cdn2.tstatic.net/palembang/foto/bank/images/darman-menunjukkan-contoh-umbi-ubi-racun.jpg",
                "http://cdn2.tstatic.net/kupang/foto/bank/images/yohanes-hale-menunjukkan-tanaman-maek-bako_20170125_220355.jpg",
                "http://2.bp.blogspot.com/-8RSONkExEbM/VSSxN0me-PI/AAAAAAAAAsk/2zYM9gK0jcs/s1600/petani-singkong-sejarah-keripik-singkong-bapake.jpg",
                "http://cdn-tin.timestechnet.com/images/2016/09/01/Petani-singkongmhzBJ.jpg",
                "https://i0.wp.com/1.bp.blogspot.com/-bc_XKL4ls-c/VRKAtuQqUgI/AAAAAAAABuY/p8aXuw4cjys/s1600/DSC01197.JPG",
                "http://cdn2.tstatic.net/bangka/foto/bank/images/budi-merawat-ubi-kasesa_20160502_140311.jpg",
                "http://hargabarangterbaru.top/wp-content/uploads/2017/01/Pembeli-dan-Penjual-UDANG-ubi.jpg",
                "https://husnulkh.files.wordpress.com/2011/12/cimg0900.jpg",
        };
        return urls;
    }

    public static String[] getKacangUrls() {
        String[] urls = new String[]{
                "https://1.bp.blogspot.com/-JTKwJTakTbg/WaUtCAqrf7I/AAAAAAAABXs/MBHk4Mr0ww8jg0kEs0-XM4N0PMqnvO_3wCLcBGAs/s1600/bawah%2BHL.jpg",
                "http://krjogja.com/kr-admin//files/news/image/43451/kacang%20tanah%20panen.jpg",
                "https://www.dictio.id/uploads/db3342/original/2X/c/c7ef4f54c350163d0efe05c22560975ddf2d173a.png",
                "http://v-images2.antarafoto.com/petani-binaan-hortindo-ne7hup-prv.jpg",
                "http://agrokomplekskita.com/wp-content/uploads/2016/12/petani-kacang-tanah.jpg",
                "http://krjogja.com/kr-admin//files/news/images/11208/Andele1.jpg",
                "http://beritadaerah.co.id/wp-content/uploads/2013/10/kacang-tanah.jpeg",
                "https://4.bp.blogspot.com/-ZoLRUVl48bY/WPwpVGwfCeI/AAAAAAAAD_M/UVKaGz2GCzMrxrNqH_PnnWYyWA1_kHh7ACLcB/s640/PETANI%2BDESA%2BBANARAN%2B%25285%2529.jpg",
                "http://4.bp.blogspot.com/-o7XIsQ_Bcrg/VMj6nvGyMNI/AAAAAAAAChs/bIaefx07P7Y/s1600/Petani%2BDesa%2BTempuran%2BBlora%2BGalau%2BKarena%2BHarga%2BKacang%2BPanjang%2BTerjun%2BBebas.jpg",
                "http://cdn2.tstatic.net/jogja/foto/bank/images/panen-kacang_2205.jpg",
                "https://store.tempo.co/cover/medium/foto/2012/01/02/s_AAN2011123003.jpg/",
                "http://www.medanbisnisdaily.com/imagesfile/201401/20140125090728_840.gif",
                "http://2.bp.blogspot.com/-rFyVqzutoUM/U-Bs-qC7jSI/AAAAAAAAFl8/BHIg_DKHpeA/s1600/llk.jpg",
                "http://assets.akurat.co/images/uploads/akurat_20170921031734_s5KP01.jpg",
                "http://www.koranmadura.com/wp-content/uploads/2015/08/petani.jpg",
        };
        return urls;
    }

    // Methods for Wishlist
    public void addWishlistImageUri(String wishlistImageUri, String mNamaW, String mLokasiW, String mLuasW) {
        this.wishlistImageUri.add(0,wishlistImageUri);
        this.wishlistNameUri.add(0,mNamaW);
        this.wishlistLokasiUri.add(0,mLokasiW);
        this.wishlistLuasUri.add(0,mLuasW);

    }

    public void removeWishlistImageUri(int position) {
        this.wishlistImageUri.remove(position);
    }

    public ArrayList<String> getWishlistImageUri(){ return this.wishlistImageUri; }
    public ArrayList<String> getWishlistNameUri(){ return this.wishlistNameUri; }
    public ArrayList<String> getWishlistLokasiUri(){ return this.wishlistLokasiUri; }
    public ArrayList<String> getWishlistLuasUri(){ return this.wishlistLuasUri; }

    // Methods for Cart
    public void addCartListImageUri(String wishlistImageUri, String nama, String lokasi, String luas) {
        this.cartListImageUri.add(0,wishlistImageUri);
        this.cartlistNameUri.add(0,nama);
        this.cartlistLokasiUri.add(0,lokasi);
        this.cartlistLuasUri.add(0,luas);
    }

    public void removeCartListImageUri(int position) {
        this.cartListImageUri.remove(position);
    }

    public ArrayList<String> getCartListImageUri(){ return this.cartListImageUri; }
    public ArrayList<String> getCartListNameUri(){ return this.cartlistNameUri; }
    public ArrayList<String> getCartListLokasiUri(){ return this.cartlistLokasiUri; }
    public ArrayList<String> getCartListLuasUri(){ return this.cartlistLuasUri; }
}

/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.allandroidprojects.ecomsample.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.allandroidprojects.ecomsample.R;
import com.allandroidprojects.ecomsample.product.ItemDetailsActivity;
import com.allandroidprojects.ecomsample.startup.MainActivity;
import com.allandroidprojects.ecomsample.utility.ImageUrlUtils;
import com.facebook.drawee.view.SimpleDraweeView;


public class ImageListFragment extends Fragment {

    public static final String STRING_IMAGE_URI = "ImageUri";
    public static final String STRING_IMAGE_POSITION = "ImagePosition";
    public static final String STRING_IMAGE_NAMA = "NamaPosition";
    public static final String STRING_IMAGE_LOKASI = "LokasiPosition";
    public static final String STRING_IMAGE_LUAS = "LuasPosition";
    public static final String STRING_IMAGE_DETAIL = "DetailPosition";
    public static final String STRING_IMAGE_ALAMAT = "AlamatPosition";
    public static final String STRING_IMAGE_DANA = "DanaPosition";

    private static MainActivity mActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (MainActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        RecyclerView rv = (RecyclerView) inflater.inflate(R.layout.layout_recylerview_list, container, false);
        setupRecyclerView(rv);
        return rv;
    }

    private void setupRecyclerView(RecyclerView recyclerView) {
      /*  if (ImageListFragment.this.getArguments().getInt("type") == 1) {
            recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        } else if (ImageListFragment.this.getArguments().getInt("type") == 2) {
            StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
            recyclerView.setLayoutManager(layoutManager);
        } else {
            GridLayoutManager layoutManager = new GridLayoutManager(recyclerView.getContext(), 3);
            layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerView.setLayoutManager(layoutManager);
        }*/

        String[] items=null;
        String[] nama=null;
        String[] lokasi=null;
        String[] luas=null;
        String[] detail=null;
        String[] alamat=null;
        String[] dana=null;
        if (ImageListFragment.this.getArguments().getInt("type") == 1){
            items =ImageUrlUtils.getSayurUrls();
            nama = ImageUrlUtils.getSayurNama();
            lokasi = ImageUrlUtils.getSayurLokasi();
            luas = ImageUrlUtils.getSayurLuas();
            detail = ImageUrlUtils.getSayurDetail();
            alamat = ImageUrlUtils.getSayurAlamat();
            dana = ImageUrlUtils.getSayurDana();
        }else if (ImageListFragment.this.getArguments().getInt("type") == 2){
            items =ImageUrlUtils.getBuahUrls();
            nama = ImageUrlUtils.getBuahNama();
            lokasi = ImageUrlUtils.getBuahLokasi();
            luas = ImageUrlUtils.getBuahLuas();
            detail = ImageUrlUtils.getBuahDetail();
            alamat = ImageUrlUtils.getBuahAlamat();
            dana = ImageUrlUtils.getBuahDana();
        }else if (ImageListFragment.this.getArguments().getInt("type") == 3){
            items =ImageUrlUtils.getSerealiaUrls();
            nama = ImageUrlUtils.getSerealiaNama();
            lokasi = ImageUrlUtils.getSerealiaLokasi();
            luas = ImageUrlUtils.getSerealiaLuas();
            detail = ImageUrlUtils.getSerealiaDetail();
            alamat = ImageUrlUtils.getSerealiaAlamat();
            dana = ImageUrlUtils.getSerealiaDana();
        }else if (ImageListFragment.this.getArguments().getInt("type") == 4){
            items =ImageUrlUtils.getUbianUrls();
            nama = ImageUrlUtils.getUbianNama();
            lokasi = ImageUrlUtils.getUbianLokasi();
            luas = ImageUrlUtils.getUbianLuas();
            detail = ImageUrlUtils.getUbianDetail();
            alamat = ImageUrlUtils.getUbianAlamat();
            dana = ImageUrlUtils.getUbianDana();
        }else if (ImageListFragment.this.getArguments().getInt("type") == 5){
            items =ImageUrlUtils.getKacangUrls();
            nama = ImageUrlUtils.getKacangNama();
            lokasi = ImageUrlUtils.getKacangLokasi();
            luas = ImageUrlUtils.getKacangLuas();
            detail = ImageUrlUtils.getKacangDetail();
            alamat = ImageUrlUtils.getKacangAlamat();
            dana = ImageUrlUtils.getKacangDana();
        }else {
            items =ImageUrlUtils.getHomeUrls();
            nama = ImageUrlUtils.getHomeNama();
            lokasi = ImageUrlUtils.getHomeLokasi();
            luas = ImageUrlUtils.getHomeLuas();
            detail = ImageUrlUtils.getHomeDetail();
            alamat = ImageUrlUtils.getHomeAlamat();
            dana = ImageUrlUtils.getHomeDana();
        }

        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(new SimpleStringRecyclerViewAdapter(recyclerView, items, nama, lokasi, luas, detail, alamat, dana));
    }

    public static class SimpleStringRecyclerViewAdapter
            extends RecyclerView.Adapter<SimpleStringRecyclerViewAdapter.ViewHolder> {

        private String[] mValues;
        private String[] mNama;
        private String[] mLokasi;
        private String[] mLuas;
        private String[] mDetail;
        private String[] mAlamat;
        private String[] mDana;
        private RecyclerView mRecyclerView;

        public static class ViewHolder extends RecyclerView.ViewHolder {
            public final View mView;
            public final SimpleDraweeView mImageView;
            public final LinearLayout mLayoutItem;
            public final ImageView mImageViewWishlist;
            public final TextView tvNama;
            public final TextView tvLokasi;
            public final TextView tvLuas;

            public ViewHolder(View view) {
                super(view);
                mView = view;
                tvNama = view.findViewById(R.id.tvNama);
                tvLuas = view.findViewById(R.id.tvKomunitas);
                tvLokasi = view.findViewById(R.id.tvAlamat);
                mImageView = (SimpleDraweeView) view.findViewById(R.id.image1);
                mLayoutItem = (LinearLayout) view.findViewById(R.id.layout_item);
                mImageViewWishlist = (ImageView) view.findViewById(R.id.ic_wishlist);
            }
        }

        public SimpleStringRecyclerViewAdapter(RecyclerView recyclerView, String[] items, String[] nama, String[] lokasi,
                                               String[] luas, String[] detail, String[] alamat, String[] dana) {
            mValues = items;
            mNama = nama;
            mLokasi = lokasi;
            mLuas = luas;
            mDetail = detail;
            mAlamat = alamat;
            mDana = dana;
            mRecyclerView = recyclerView;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onViewRecycled(ViewHolder holder) {
            if (holder.mImageView.getController() != null) {
                holder.mImageView.getController().onDetach();
            }
            if (holder.mImageView.getTopLevelDrawable() != null) {
                holder.mImageView.getTopLevelDrawable().setCallback(null);
//                ((BitmapDrawable) holder.mImageView.getTopLevelDrawable()).getBitmap().recycle();
            }
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
           /* FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) holder.mImageView.getLayoutParams();
            if (mRecyclerView.getLayoutManager() instanceof GridLayoutManager) {
                layoutParams.height = 200;
            } else if (mRecyclerView.getLayoutManager() instanceof StaggeredGridLayoutManager) {
                layoutParams.height = 600;
            } else {
                layoutParams.height = 800;
            }*/
            final Uri uri = Uri.parse(mValues[position]);
            holder.mImageView.setImageURI(uri);
            holder.mLayoutItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mActivity, ItemDetailsActivity.class);
                    intent.putExtra(STRING_IMAGE_URI, mValues[position]);
                    intent.putExtra(STRING_IMAGE_POSITION, position);
                    intent.putExtra(STRING_IMAGE_NAMA, mNama[position]);
                    intent.putExtra(STRING_IMAGE_LOKASI, mLokasi[position]);
                    intent.putExtra(STRING_IMAGE_DETAIL, mDetail[position]);
                    intent.putExtra(STRING_IMAGE_ALAMAT, mAlamat[position]);
                    intent.putExtra(STRING_IMAGE_DANA, mDana[position]);
                    intent.putExtra(STRING_IMAGE_LUAS, mLuas[position]);

                    mActivity.startActivity(intent);

                }
            });

            if(mNama !=null) {
                holder.tvNama.setText(mNama[position]);
                holder.tvLokasi.setText(mLokasi[position]);
                holder.tvLuas.setText(mLuas[position]);
            }

            //Set click action for wishlist
            holder.mImageViewWishlist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ImageUrlUtils imageUrlUtils = new ImageUrlUtils();
                    imageUrlUtils.addWishlistImageUri(mValues[position],mNama[position],mLokasi[position],mLuas[position]);
                    holder.mImageViewWishlist.setImageResource(R.drawable.ic_favorite_black_18dp);
                    notifyDataSetChanged();
                    Toast.makeText(mActivity,"Item added to wishlist.",Toast.LENGTH_SHORT).show();

                }
            });

        }

        @Override
        public int getItemCount() {
            return mValues.length;
        }
    }
}

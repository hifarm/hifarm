package com.allandroidprojects.ecomsample.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.allandroidprojects.ecomsample.Berita;
import com.allandroidprojects.ecomsample.ListAdapter;
import com.allandroidprojects.ecomsample.R;
import com.allandroidprojects.ecomsample.ShowBeritaActivity;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment {

    private ListView listView;
    List<Berita> Beritas = new ArrayList<>();
    int dataSize;
    public Berita listBerita = new Berita();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_home);
      //  initInstances();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_home,container,false);
        prepareData();

        ListAdapter adapter = new ListAdapter(getContext(), listBerita);
        listView = (ListView) rootView.findViewById(R.id.list_itemberita);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(listViewClickListener);
//        initInstances();
        return rootView;
    }

    private void setContentView(int fragment_home) {
    }

    private void initInstances() {

    }

    private void prepareData() {

        int resId[] = {R.drawable.berita1, R.drawable.berita2, R.drawable.berita3, R.drawable.berita4
                , R.drawable.berita5};

        String breed[] = {"Kementan Kembali Ekspor Bawang Merah ke Thailand dan Singapura", "HKTI Jadikan Bali Proyek Percontohan Benih Padi Nasional", "NTT Bakal Ekspor Jagung ke Timor Leste",
                            "Kebijakan Pemerintah Dinilai Justru Mematikan Petani, Investasi Pertanian Menurun ", "RI-Senegal Pererat Kerja Sama Sektor Pertanian"};

        String description[] = {getString(R.string.berita1), getString(R.string.berita2), getString(R.string.berita3), getString(R.string.berita4)
                , getString(R.string.berita5),};
        dataSize = resId.length;


        Log.d("khem", "dataSize " + resId.length);
        Log.d("khem", "breed " + resId.length);
        Log.d("khem", "description " + resId.length);


        for (int i = 0; i < dataSize; i++) {
            Log.d("khem", " " + i);
            Berita Berita = new Berita(resId[i], breed[i], description[i]);
            Beritas.add(Berita);
        }

        listBerita.setBeritas(Beritas);

        //Log.d("khem",listBerita);
    }

    /*************************
     * Listener
     ***************************/

    AdapterView.OnItemClickListener listViewClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Intent intent = new Intent(HomeFragment.super.getContext(),ShowBeritaActivity.class);
            intent.putExtra("resId",listBerita.getBeritas().get(position).getResId());
            intent.putExtra("breed",listBerita.getBeritas().get(position).getBreed());
            intent.putExtra("desc",listBerita.getBeritas().get(position).getDescription());

            startActivity(intent);
        }
    };
}

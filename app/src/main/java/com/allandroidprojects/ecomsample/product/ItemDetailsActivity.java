package com.allandroidprojects.ecomsample.product;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.allandroidprojects.ecomsample.R;
import com.allandroidprojects.ecomsample.fragments.ImageListFragment;
import com.allandroidprojects.ecomsample.fragments.ViewPagerActivity;
import com.allandroidprojects.ecomsample.notification.NotificationCountSetClass;
import com.allandroidprojects.ecomsample.startup.MainActivity;
import com.allandroidprojects.ecomsample.utility.ImageUrlUtils;
import com.facebook.drawee.view.SimpleDraweeView;

public class ItemDetailsActivity extends AppCompatActivity {
    int imagePosition;
    String stringImageUri;
    String detail;
    String nama;
    String alamat;
    String dana;
    String lokasi;
    String luas;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_details);
        SimpleDraweeView mImageView = (SimpleDraweeView)findViewById(R.id.image1);
        TextView textViewAddToCart = (TextView)findViewById(R.id.text_action_bottom1);
        TextView tvNama = findViewById(R.id.tvNama);
        TextView tvDetail = findViewById(R.id.tvDetail);
        TextView tvAlamat = findViewById(R.id.tvAlamat);
        TextView tvDana = findViewById(R.id.tvDana);
        //Getting image uri from previous screen
        if (getIntent() != null) {
            stringImageUri = getIntent().getStringExtra(ImageListFragment.STRING_IMAGE_URI);
            imagePosition = getIntent().getIntExtra(ImageListFragment.STRING_IMAGE_URI,0);
            nama = getIntent().getStringExtra(ImageListFragment.STRING_IMAGE_NAMA);
            detail = getIntent().getStringExtra(ImageListFragment.STRING_IMAGE_DETAIL);
            alamat = getIntent().getStringExtra(ImageListFragment.STRING_IMAGE_ALAMAT);
            dana = getIntent().getStringExtra(ImageListFragment.STRING_IMAGE_DANA);
            lokasi = getIntent().getStringExtra(ImageListFragment.STRING_IMAGE_LOKASI);
            luas = getIntent().getStringExtra(ImageListFragment.STRING_IMAGE_LUAS);
        }
        tvNama.setText(nama);
        tvDetail.setText(detail);
        tvAlamat.setText(alamat);
        tvDana.setText(dana);
        Uri uri = Uri.parse(stringImageUri);
        mImageView.setImageURI(uri);
        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Intent intent = new Intent(ItemDetailsActivity.this, ViewPagerActivity.class);
                    intent.putExtra("position", imagePosition);
                    startActivity(intent);

            }
        });

        textViewAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageUrlUtils imageUrlUtils = new ImageUrlUtils();
                imageUrlUtils.addCartListImageUri(stringImageUri,nama,lokasi,luas);
                Toast.makeText(ItemDetailsActivity.this,"Investment Success.",Toast.LENGTH_SHORT).show();
                MainActivity.notificationCountCart++;
                NotificationCountSetClass.setNotifyCount(MainActivity.notificationCountCart);
            }
        });

        /*textViewBuyNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageUrlUtils imageUrlUtils = new ImageUrlUtils();
                imageUrlUtils.addCartListImageUri(stringImageUri);
                MainActivity.notificationCountCart++;
                NotificationCountSetClass.setNotifyCount(MainActivity.notificationCountCart);
                startActivity(new Intent(ItemDetailsActivity.this, CartListActivity.class));

            }
        });*/

    }
}
